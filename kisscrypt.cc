// A set of crypto routines by Joel Odom.  This project has two goals.  (1) Do crypto right,
// and (2) keep the implementation as simple as possible.

// Build: g++ -Wall -o kisscrypt kisscrypt.cc -x c aes256.c

#include <cstdlib>
#include <iostream>
#include "aes256.h"

#define AES_KEY_LENGTH 256


#define ASSERT(b) assert(b, __LINE__)

void assert(bool b, unsigned int line)
{
  if (!b)
  {
    std::cout << "Assert failure at line " << line << "." << std::endl;
    exit(-1);
  }
}

void test_simple_encrypt_decrypt()
{
  const uint8_t C = 0xf9; // arbitrary

  // initialize a key

  uint8_t key[32];
  memset(key, C, sizeof(key));

  // populate a buffer

  uint8_t buf[16];
  memset(buf, C, sizeof(buf));

  for (size_t i = 0; i < sizeof(buf); ++i)
    ASSERT(buf[i] == C);

  // permute the buffer

  {
    aes256_context ctx;
    aes256_init(&ctx, key);
    aes256_encrypt_ecb(&ctx, buf);
  }

  // check that the buffer changed
  // this is a poor test that could theoretically fail

  uint8_t x = 0;
  for (size_t i = 0; i < sizeof(buf); ++i)
    x |= (buf[i] ^ C);
  ASSERT(x != 0);

  // reverse the permutation

  {
    aes256_context ctx;
    aes256_init(&ctx, key);
    aes256_decrypt_ecb(&ctx, buf);
  }

  for (size_t i = 0; i < sizeof(buf); ++i)
    ASSERT(buf[i] == C);
}

void all_tests()
{
  ASSERT(true);
  test_simple_encrypt_decrypt();
}

int main(int argc, char **argv)
{
  all_tests();
  std::cout << "Done." << std::endl;
  return 0;
}
